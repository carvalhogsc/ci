module.exports = {
  api_server: "https://petstore.swagger.io/v2",
  putData: {
      "id": 1,
      "category": {
        "id": 1,
        "name": "string"
      },
      "name": "doggieram",
      "photoUrls": [
        "string"
      ],
      "tags": [
        {
          "id": 1,
          "name": "string"
        }
      ],
      "status": "available"
  },
  postData : {
      "id": 1291, 
      "category": {
        "id": 1,
        "name": "string"
      },
      "name": "seba",
      "photoUrls": [
        "string"
      ],
      "tags": [
        {
          "id": 1,
          "name": "string"
        }
      ],
      "status": "available"
    }
}
