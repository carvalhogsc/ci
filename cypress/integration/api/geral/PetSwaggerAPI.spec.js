/// <reference types="cypress" />
var data = require('../../../fixtures/dataPetSwagger')

const requestGetPet = (petID = 0) => {
    return cy.request({
        method: 'GET',
        headers: {
            api_key: "05121991",
        },
        url: data.api_server+"/pet/"+petID,
        failOnStatusCode: false
    })                    
}

context('Swagger Petstore PET', () => {
    it('Cadastrar novo pet', () => {
        cy.request({
            method: 'POST',
            headers: {
                api_key: "05121991",
            },
            url: data.api_server+"/pet/",
            failOnStatusCode: false,
            body: data.postData
        }).then(result => {
            expect(result.status).to.be.equals(200)            
        })

        requestGetPet(data.postData.id).then(result => {
            expect(result.status).to.be.equals(200)
            expect(result.body.name).to.be.equals(data.postData.name)

        })

        
    })



    it('Testar Content-Type', () => {
        requestGetPet(data.postData.id).then(result => {
            expect(result.headers["content-type"]).to.be.equals("application/json")
        })
    })

    it('Pegar Pet válido', () => {
        
        requestGetPet(data.postData.id).then(result => {
            expect(result.status).to.be.equals(200)
            expect(result.body.id).to.be.equals(data.postData.id)
        })
    })

    it('Pegar Pet inválido', () => {
        requestGetPet(5121991).then(result => {
            expect(result.status).to.be.equals(404)
            expect(result.body.message).to.equals("Pet not found")
        })
    })

    it('Atualizar Pet', () => {
        cy.request({
            method: 'PUT',
            headers: {
                api_key: "05121991",
            },
            url: data.api_server+"/pet/",
            failOnStatusCode: false,
            body: data.putData
        }).then(result => {
            expect(result.status).to.be.equals(200)
            
        })

        requestGetPet(data.putData.id).then(result => {
            expect(result.status).to.be.equals(200)
            expect(result.body.name).to.be.equals("doggieram")                    
        })
    })

    it('Deletar PET',() => {
        cy.request({
            method: 'DELETE',
            headers: {
                api_key: "05121991",
            },
            url: data.api_server+"/pet/"+data.postData.id,
            failOnStatusCode: false,
        }).then(result => {
            expect(result.status).to.be.equals(200)            
        })

        requestGetPet(data.postData.id).then(result => {
            expect(result.status).to.be.equals(404)
        })
    })

    it('Procurar por Status PET',() => {
        cy.request({
            method: 'GET',
            headers: {
                api_key: "05121991",
            },
            url: data.api_server+"/pet/findByStatus?status=available",
            failOnStatusCode: false,
        }).then(result => {
            expect(result.status).to.be.equals(200)
            result.body.map(element => {
                expect(element['status']).to.equals('available')
            })        
        })

    })
})