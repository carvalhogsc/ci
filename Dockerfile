FROM node:latest AS cypress
WORKDIR /home/node/
COPY . .
RUN apt-get update -y && apt-get install -y xterm libgtk2.0-0 libgtk-3-0 libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb
RUN npm install cypress --save-dev
EXPOSE 3000
VOLUME ["/home/node/"]
